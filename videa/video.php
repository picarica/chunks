<?php
/*
    // video nastavenia
    $video_pw; // ukazovatel pre processwire field pre videa
    $video_div; // String kde bude div wrapper pre videa 
    include('./chunks/videa/video.php');
*/

/*
Lightcase init.

na includovat

<link href="<?=$config->urls->templates;?>chunks/videa/lightcase/src/css/lightcase.min.css" rel="stylesheet" />

<script type="text/javascript" src="<?= $config->urls->templates; ?>chunks/videa/lightcase/src/js/lightcase.min.js"></script>
<script type="text/javascript" src="<?= $config->urls->templates; ?>chunks/videa/lightcase/vendor/jQuery/jquery.events.touch.min.js"></script>

jQuery(document).ready(function($) {
	$('a[data-rel^=lightcase]').lightcase({
		swipe: true,
		controls : true,
		autobuffer : true,
		speedIn:0,
		speedOut:0,
		maxWidth:3840,
		maxheight:2160,
		onStart : {
			function() {
				var i, iframe;
				iframe = document.querySelector(".lightcase-contentInner > iframe");
				iframe.setAttribute('allowFullScreen', '')
				iframe.setAttribute('webkitallowfullscreen', '')
				iframe.setAttribute('mozallowfullscreen', '')
				var zdroj = iframe.src ;
				iframe.removeAttribute("src") ;
				iframe.src = zdroj ;
			}
		}
	});
});

*/

//zmazanie nahladov pre video keby sa daco pokazi
/* function deleteDirectory($dir) {
    system('rm -r -- ' . escapeshellarg($dir), $retval);
    return $retval == 0; // UNIX commands return zero on success
}
deleteDirectory("thumbnails");
 */
if ($video_pw) {
		$text = trim($video_pw);
		//echo "***<br/>$text<br/>***";
		$array = explode(PHP_EOL, $text);
		$aa = 1;
        if (isset($video_div)) {
            echo $video_div;
        }
		for ($i = 0; $i < count($array); $i++) {
			// echo "<section>";
			// Get image form video URL
			$url = $array[$i];
			$urls = parse_url($url);
			$typ_videa = 'ziadne';
			if (preg_match('%youtube|youtu\.be%i', $urls['host'])) {
				$typ_videa = 'youtube';
			} elseif (preg_match('%vimeo%i', $urls['host'])) {
				$typ_videa = 'vimeo';
			} else {
				continue;
			}
			// echo $typ_videa;
			if ($typ_videa == 'youtube') {

				//$video_id = explode("?v=", $url); // For videos like http://www.youtube.com/watch?v=...
				//if (empty($video_id[1]))
    			//	$video_id = explode("/v/", $url); // For videos like http://www.youtube.com/watch/v/..

				//$video_id = explode("&", $video_id[1]); // Deleting any other params
				//$video_id = $video_id[0];
				
				//echo $video_id;


				//Expect the URL to be http://youtu.be/abcd, where abcd is the video ID
				if ($urls['host'] == 'youtu.be') :
					$imgPath = ltrim($urls['path'], '/');
				//Expect the URL to be http://www.youtube.com/embed/abcd
				elseif (strpos($urls['path'], 'embed') == 1) :
					$imgPath = end(explode('/', $urls['path']));
				//Expect the URL to be http://www.youtube.com/shorts/abcd
				elseif (strpos($urls['path'], 'shorts') == 1) :
					$temper = explode('/', $urls['path']);
					$imgPath = end($temper);
				//Expect the URL to be abcd only
				elseif (strpos($url, '/') === false) :
					$imgPath = $url;
				//Expect the URL to be http://www.youtube.com/watch?v=abcd
				else :
					parse_str($urls['query'], $v);
					$imgPath = implode("",$v);
				endif;
				
				//id videa
				//echo $imgPath;

				$imgurl = "https://i1.ytimg.com/vi/{$imgPath}/maxresdefault.jpg";



				if ($urls['host'] == 'youtu.be') :
					$url = "https://www.youtube.com/watch?v={$imgPath}";
				endif;
				 $url = "https://www.youtube.com/embed/{$imgPath}?&autoplay=1";


			}
			if ($typ_videa == 'vimeo') {
				$imgid = ltrim($urls['path'], '/');
				$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$imgid.php"));
				$imgurl = $hash[0]['thumbnail_large'];
				$url = "https://player.vimeo.com/video/{$imgid}";
			}

			echo "<a data-rel=lightcase:videjko' href='$url'  class='image vid";

			
			echo "' rel='$aa' title='Kliknite pre spustenie'>";
			if ($typ_videa == 'vimeo') {
				echo "<img class='' loading='lazy' src='$imgurl' alt='video'>";
			} else {
				$imgPath_short = substr($imgPath, 0, 11);
				if (file_exists("thumbnails/" .$imgPath_short)) {
					if (filesize("thumbnails/" .$imgPath_short) < 4000) {
						unlink("thumbnails/" .$imgPath_short);
					}
					list($width, $height, $type, $attr) = getimagesize("thumbnails/" .$imgPath_short);
					echo "<img loading='lazy' src='{$config->urls->templates}thumbnails/$imgPath_short' alt='video'>";
				} else {
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,$imgurl);
					// don't download content
					curl_setopt($ch, CURLOPT_NOBODY, 1);
					curl_setopt($ch, CURLOPT_FAILONERROR, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'); 	
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					// docasne settingy pre debuggovanie
					curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
					
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1); 
					curl_setopt($ch, CURLOPT_TIMEOUT, 1); //timeout in seconds
					$result = curl_exec($ch);
					#$result = FALSE;
					curl_close($ch);
					if($result !== FALSE) {
							// Initialize the cURL session
							$ch = curl_init($imgurl);
						
							// Initialize directory name where
							// file will be save
							$dir =  "thumbnails/";
							if ( !file_exists( $dir ) && !is_dir( $dir ) ) {
									mkdir( $dir );
							} 
							// Use basename() function to return
							// the base name of file
							$file_name = basename($imgPath_short);
							$file_name_fix = substr($file_name, 0, 11);
							// Save file into file location
							$save_file_loc = $dir . $file_name_fix;
							// Open file
							$fp = fopen($save_file_loc, 'wb');
							
							// It set an option for a cURL transfer
							curl_setopt($ch, CURLOPT_FILE, $fp);
							curl_setopt($ch, CURLOPT_HEADER, 0);
							
							// Perform a cURL session
							curl_exec($ch);
							
							// Closes a cURL session and frees all resources
							curl_close($ch);
							
							// Close file
							fclose($fp);
	
					} else {
							// Initialize the cURL session
							$ch = curl_init("https://i1.ytimg.com/vi/{$imgPath_short}/hqdefault.jpg");

							// Initialize directory name where
							// file will be save
							$dir =  "thumbnails/";
							if ( !file_exists( $dir ) && !is_dir( $dir ) ) {
									mkdir( $dir );
							} 
							// Use basename() function to return
							// the base name of file
							$file_name = basename($imgPath_short);
							$file_name_fix = substr($file_name, 0, 11);
							// Save file into file location
							$save_file_loc = $dir . $file_name_fix;
							// Open file
							$fp = fopen($save_file_loc, 'wb');
							
							// It set an option for a cURL transfer
							curl_setopt($ch, CURLOPT_FILE, $fp);
							curl_setopt($ch, CURLOPT_HEADER, 0);
							
							// Perform a cURL session
							curl_exec($ch);
							
							// Closes a cURL session and frees all resources
							curl_close($ch);
							
							// Close file
							fclose($fp);
					}
					echo "<img class='' loading='lazy' src='{$config->urls->templates}thumbnails/$imgPath_short' alt='video'>";
				}

					/*
					{
						echo "<img class='' loading='lazy' src='$imgurl' alt='video'>";

					}
					else
					{*/
						/* echo "<div style='height:0px;padding-bottom:56.23%;background:url(https://i1.ytimg.com/vi/{$imgPath}/hqdefault.jpg) no-repeat center; 
						background-size:cover;' loading='lazy' alt='video' ></div>"; */
					/*} 
					/* if ($user->isLoggedin()) {
						if (strstr(current(get_headers($imgurl)), "200")) {
							echo "existuje";
						} else {
							echo "chyba";
						
					} */
			}
			echo '<span class="image vid ';
			if ($typ_videa == 'vimeo') {
				echo "vimeo";
			} else {
				echo "youtube";
			}
			echo '"></span></a>'; 
			$aa++;
		}
        if (isset($video_div)){
		    echo "</div>";
        }
	}
?>
