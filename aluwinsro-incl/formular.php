

<?php

// define variables and set to empty values
$nameErr = $emailErr = $personErr = $ansErr = $numErr = "";
$name = $email = $person = $ans = $num = "";


if ($pages->get("/nastavenie/")->captcha) {
    $captcha = $pages->get("/nastavenie/")->captcha;
    $answer = explode("|", $captcha);
}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function remove_diacriti($sentence) {
    $patterns = array();
    $patterns[0] = '/á/';
    $patterns[1] = '/ä/';
    $patterns[2] = '/č/';
    $patterns[3] = '/ď/';
    $patterns[4] = '/é/';
    $patterns[5] = '/í/';
    $patterns[6] = '/ĺ/';
    $patterns[7] = '/ľ/';
    $patterns[8] = '/ň/';
    $patterns[9] = '/ó/';
    $patterns[10] = '/ô/';
    $patterns[11] = '/ö/';
    $patterns[12] = '/ő/';
    $patterns[13] = '/ŕ/';
    $patterns[14] = '/š/';
    $patterns[15] = '/ť/';
    $patterns[16] = '/ú/';
    $patterns[17] = '/ü/';
    $patterns[18] = '/ű/';
    $patterns[19] = '/ý/';
    $patterns[20] = '/ž/';
    $replacements = array();
    $replacements[0] = 'a';
    $replacements[1] = 'a';
    $replacements[2] = 'c';
    $replacements[3] = 'd';
    $replacements[4] = 'e';
    $replacements[5] = 'i';
    $replacements[6] = 'l';
    $replacements[7] = 'l';
    $replacements[8] = 'n';
    $replacements[9] = 'o';
    $replacements[10] = 'o';
    $replacements[11] = 'o';
    $replacements[12] = 'o';
    $replacements[13] = 'r';
    $replacements[14] = 's';
    $replacements[15] = 't';
    $replacements[16] = 'u';
    $replacements[17] = 'u';
    $replacements[18] = 'u';
    $replacements[19] = 'y';
    $replacements[20] = 'z';
    $bezdia = preg_replace($patterns, $replacements,$sentence);
    return $bezdia;
}

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        echo '
        ';

        if (empty($_POST["meno"])) {
            $nameErr = "* Meno je povinný údaj";
        } else {
            $name = test_input($_POST["meno"]);
        }
        
        if (empty($_POST["email"])) {
            $emailErr = "* E-mail je povinný údaj";
        } else {
            $email = test_input($_POST["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Zly formát E-mailu";
              }
        }

        if (empty($_POST["num"])) {
            $numErr = "* Tel. číslo je povinný údaj";
        } else {
            $num = test_input($_POST["num"]);
            $numcorr = filter_var($num, FILTER_SANITIZE_NUMBER_INT);
            if (!preg_match('/^[0-9]{10}+$/', $numcorr)) {
                $numErr = "Použite format čísla 09xx xxx xxx";
              }
        }

        if (empty($_POST["suhlas"])) {
            $personErr = "* Súhlasenie so spracovaním osobných údajov je povinné";
        } else {
            $person = test_input($_POST["suhlas"]);
        }

        if ($pages->get("/nastavenie/")->captcha) {
            if (empty($_POST["ans"])) {
                $ansErr = "Odpoveď je prázdna";
            } else {
            $ans = test_input($_POST["ans"]);
            //najskor vycistit input, aby tam neboli medzeri, tabulatory, atd,
            $clean_input = preg_replace('/\s/u', '', $ans); 
            $fixans = mb_strtolower($clean_input,'UTF-8');
            $nodians = remove_diacriti($fixans);
            
            
            $testanswer = test_input($answer[1]);
            $fixequestion = preg_replace('/\s/u', '',$testanswer);
            $lowerquestion = mb_strtolower($fixequestion,'UTF-8');

            $diakritikaprecquestion = remove_diacriti($lowerquestion);
            if ($nodians !== $diakritikaprecquestion) {
                $ansErr = "Zle zadaná odpoveď!";
            }
                
            }
        }
    

        $to = 'aluwin@aluwin.sk';
        #$to = 'picarica456@gmail.com';
        if ($page->url == '/kontakt/') {
            $subject = 'Formular z aluwin.sk z kontaktu';
        } else {
            $subject = 'Formular z aluwin.sk z homepage';
        }
        

        $name = $_POST['meno'];
        $email = $_POST['email'];
        $message = $_POST['text'] . "tel. zakaznika:  " . $_POST['num'];

        $headers = 'From:'. $email . "\r\n" .
        'Reply-To:'. $email . "\r\n";


        //honey pot field
        $honeypot = $_POST['nick'];
        $header = "From: $name <$name>";

        //check if the honeypot field is filled out. If not, send a mail.
        #pouziva sa ja pri footeri
        $errorcheck = !empty($_POST["suhlas"]) && !empty($_POST["email"]) && !empty($_POST["meno"]) && empty($emailErr) && empty($numErr) && empty($ansErr);
        if(!empty($honeypot) ){
            return; //you may add code here to echo an error etc.
        }elseif ($errorcheck) {
            mail( $to, $subject, $message , $headers );
            echo "
            <div id='backg' >
                <div class='popup'>
                    <i id='box' class='fa fa-window-close'></i>
                    <i class='fa fa-envelope'></i>".
                    $pages->get('/nastavenie/')->form_text .
                "</div>
            </div>
            ";
        }
    }



?>


<?php if ($page->url == '/kontakt/'): ?>
<div class="">

    <form method="POST" action="<?=$page->url()?>" id="my-form" class="form_5" enctype="multipart/form-data">

        <input style="display:none;" type="text" name="nick" value="">

        <div class="e2_htmlcontent">
            <h2>Napíšte nám</h2>
            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="text" id="meno" name="meno" required="" value="<?php if (!empty($_POST['meno'])) {echo $_POST['meno'];}?>">
                <label class="input__label input__label--fumi" for="meno">
                    <i class="fa fa-fw fa-user icon icon--fumi"></i>
                    <span class="input__label-content input__label-content--fumi">Vaše meno a priezvisko <b>*</b></span>
                </label>
            </span>
            <p style="color:red;"><?=$nameErr?></p>
            

            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="email" id="email" name="email" required="" value="<?php if (!empty($_POST['email'])) {echo $_POST['email'];}?>">
                <label class="input__label input__label--fumi" for="email">
                    <i class="fa fa-fw fa-envelope icon icon--fumi"></i>
                    <span class="input__label-content input__label-content--fumi">E-mail kontakt <b>*</b></span>
                </label>
            </span>
            <p style="color:red;"><?=$emailErr?></p>

            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="tel" id="num" name="num" required="" value="<?php if (!empty($_POST['num'])) {echo $_POST['num'];}?>">
                <label class="input__label input__label--fumi" for="num">
                    <i class="fa fa-fw fa-phone icon icon--fumi"></i>
                    <span class="input__label-content input__label-content--fumi">Váš telefón <b>*</b></span>
                </label>
            </span>
            <p style="color:red;"><?=$numErr?></p>

            <span class="input input--fumi">
                <textarea class="input__field input__field--fumi" cols="18" id="text" name="text" rows="4" ><?php if (!empty($_POST['text'])) {echo $_POST['text'];}?></textarea>
                <label class="input__label input__label--fumi" for="text">
                    <i class="fa fa-fw fa-comment icon icon--fumi"></i>
                    <span class="input__label-content input__label-content--fumi">Správa</span>
                </label>
            </span>
            <p class="formular_popis"><span>*</span> - povinné položky</p>
        </div>
        <?php
        if($pages->get("/nastavenie/")->captcha) {
            echo "<p><b>Overovacia otázka<br></b>";
            echo $answer[0];
            
            echo "</p>";
            echo '
            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="text" id="ans" name="ans" required="" >
                <label class="input__label input__label--fumi" for="ans">
                    <i class="fa fa-fw fa-eye icon icon--fumi"></i>
                    <span class="input__label-content input__label-content--fumi">Odpoved <b>*</b></span>
                </label>
            </span>
            <p style="color:red;">'. $ansErr .'</p>

            ';
        }
        ?>
        <p style="margin-top:5px;"><input id="suhlas" name="suhlas" required="" style="margin-right: 5px;"
                type="checkbox">Súhlasím so <a style="font-style: italic;" target="_blank"
                href="/ochrana-osobnych-udajov">spracovaním osobných údajov</a> <b style="color:red;">*</b></p>
                <p style="color:red;"><?=$personErr?></p>

        <button class="g-recaptcha formular_button" data-callback='onSubmit' type="submit" data-action='submit' data-sitekey="6LdPhoYfAAAAAD4F4oPVMnocdHM0tcP9E-m5_4N_">ODOSLAŤ SPRÁVU</button>






    </form>
</div>

<?php elseif ($page->url == '/'): ?>


<div class="map_contact">
    <div class="box_margin">
        <h3>Napíšte nám</h3>
    </div>


    <form method="POST" action="<?=$page->url()?>" id="my-form" class="form_5" enctype="multipart/form-data">
        <input style="display:none;" type="text" name="nick" value="">

        <div class="e2_htmlcontent">
            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="text" id="meno" name="meno" required="" value="<?php if (!empty($_POST['meno'])) {echo $_POST['meno'];}?>">
                <label class="input__label input__label--fumi" for="meno">
                    <span class="input__label-content input__label-content--fumi">Meno a priezvisko <b style="color:red;">*</b></span>
                </label>
            </span>
            <p style="color:red;"><?=$nameErr?></p>

            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="email" id="email" name="email" required="" value="<?php if (!empty($_POST['email'])) {echo $_POST['email'];}?>">
                <label class="input__label input__label--fumi" for="email">
                    <span class="input__label-content input__label-content--fumi">E-mail <b style="color:red;">*</b></span>
                </label>
            </span>
            <p style="color:red;"><?=$emailErr?></p>

            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="tel" id="num" name="num" required="" value="<?php if (!empty($_POST['num'])) {echo $_POST['num'];}?>">
                <label class="input__label input__label--fumi" for="num">
                    <span class="input__label-content input__label-content--fumi">Tel. <b style="color:red;">*</b></span>
                </label>
            </span>
            <p style="color:red;"><?=$numErr?></p>

            <span class="input input--fumi">
                <textarea class="input__field input__field--fumi" cols="18" id="text" name="text" rows="4"><?php if (!empty($_POST['text'])) {echo $_POST['text'];}?></textarea>
                <label class="input__label input__label--fumi" for="text">
                    <span class="input__label-content input__label-content--fumi">Text správy</span>
                </label>
            </span>
        </div>
        <?php
        if($pages->get("/nastavenie/")->captcha) {
            echo "<p><b>Overovacia otázka<br></b>";
            echo $answer[0];
            
            echo "</p>";
            echo '
            <span class="input input--fumi">
                <input class="input__field input__field--fumi" type="text" id="ans" name="ans" required="">
                <label class="input__label input__label--fumi" for="ans">
                    <span class="input__label-content input__label-content--fumi">Odpoved <b style="color:red;">*</b></span>
                </label>
            </span>
            <p style="color:red;">'. $ansErr .'</p>

            ';
        }
        ?>
        <p class="hiddd" style="margin-top:5px;"><input id="suhlas" name="suhlas" required="" style="margin-right: 5px;"
                type="checkbox">Súhlasím so <a style="font-style: italic;" target="_blank"
                href="/ochrana-osobnych-udajov">spracovaním osobných údajov</a> <b style="color:red;">*</b></p>
                <p style="color:red;"><?=$personErr?></p>


                <button data-badge="inline" class="formular_button" data-callback='onSubmit' type="submit" data-action='submit' data-sitekey="6LdPhoYfAAAAAD4F4oPVMnocdHM0tcP9E-m5_4N_">ODOSLAŤ SPRÁVU</button>
    </form>

    <?php else :
echo "Chyba";
endif;

?>

</div>

<script>
function onSubmit(token) {
    document.getElementById("my-form").submit();
}


</script>
<script>
var box = document.getElementById("box")
var elemn = document.getElementById("backg")

<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if ($errorcheck) {

                
        echo "        
        setTimeout(function(){
            elemn.classList.add(\"fadeout\");
            setTimeout(function(){
                elemn.remove()
            }, 260);//wait 2 seconds
        }, 30000);//wait 2 seconds

        box.addEventListener('click', function handleClick(event) {
        elemn.classList.add(\"fadeout\");
        setTimeout(function(){
                elemn.remove()
            }, 260);//wait 2 seconds
        });
        var element = document.querySelector(\"#my-form\");

        element.scrollIntoView({ behavior: 'smooth', block: 'end'});
        ";
        } else {
            echo "
            var element = document.querySelector(\"#my-form\");

            element.scrollIntoView({ behavior: 'smooth', block: 'end'});";
    
        }
    }
?>

</script>


