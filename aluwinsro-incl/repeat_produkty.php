<?php
$backgroundrgba = "";
if ($rep->onoff_rgba == 1) {
    if (!empty($rep->r_rgba)) {
        $backgroundrgba = $rep->r_rgba;
        // je zapnuty a farba z repeat fieldu
    } else {
        $backgroundrgba = $pages->get(1034)->set_rgba;
        // je zapnuty pouzije rgba zo settingu
    }
} 
 echo "<div style='background-color:rgba($backgroundrgba);' class='background-repeat'>";
//SLIDER
if ($string == "image_slider" || $tri == 0) {
    if (count($rep->image_slider)) {
        echo "<div class='swiper'><div class=' pod_swiper'>
        <div class='swiper-pagination'></div>

        <div class='swiper-wrapper'>
        ";
        foreach($rep->image_slider as $image) {
            $options = array('quality' => 80, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
            #$large = $image->size(1140, 500);
            #echo "<img class=\"swiper-slide\" src='$large->webpUrl'>";
            echo "<div class=\"swiper-slide\">";
            echo "<img  src='$image->webpUrl'>";

            if ($image->akcia_global_color_fix || $image->home_slider_text ) {
				echo "<div class='gradient-back'></div>";
			}
		if($image->description) {
			echo "
			<div class='rslider_content'>
			<div class='container text-center'>
			<div class='rslider_content_in'>$image->description</div>
			</div>
			</div>";
		}
		

			if ($image->akcia_global_color_fix || $image->home_slider_text ) {
			
				echo '
				<div class="posi">';
				if ($image->akcia_global_color_fix ) {

					$farba = $image->akcia_global_color_fix;
	
					//najskor vycistit input, aby tam neboli medzeri, tabulatory, atd,
	
					//rozdelit hodnoty podla ciarky, jeden input za ciarkou atd.
					$farby = explode("|", $farba);

				echo	'<div style="background-color:'.preg_replace('/\s/u', '', $farby[0]).';" class="home action">
						<span style="color:'.preg_replace('/\s/u', '', $farby[1]).';">'. $farby[2] .'</span>
					</div>';
				}
				if ($image->home_slider_text ) {

					echo'
					<div class="headline">
						'.$image->home_slider_text.'
					</div>';
                    if ($image->home_slider_button_text) {
                        echo '<a class="button" href="'.$image->reference->url.'">'.$image->home_slider_button_text.'</a>';
                    }
				}
				echo '</div>
				';
			}
            echo "</div>";
        }
        
        echo "</div>";
        echo "<div class='swiper-button-prev'></div>
        <div class='swiper-button-next'></div>";
        echo "</div>"; # pod swiper div
        echo "</div>"; # swiper div
    }
}

// HTML5 video
if ($string == "local_video" || $tri == 0) {
    if ($rep->local_video) {

		$urlvideo = $rep->local_video->url;
		$koncovka = substr($urlvideo, strrpos($urlvideo, '.' )+1)."\n";

		echo "
        <div class='repeat_video'>
		<video playsinline muted autoplay loop playsinline>
		
			<source src='$urlvideo' type='video/$koncovka'>
		</video>
        </div>";
    }
}



// TELO BODY

if ($string == "body" || $tri == 0) {
    echo $rep->body;
}



//TITLE GALERIA
if ($string == "r_gallery_title" || $tri == 0){
    if ($rep->r_gallery_title) {
        echo "<h2>$rep->r_gallery_title</h2>";
    }
}

// NASTEAVNIE VELKOSTI GALERII
if ($string == "r_gallery" || $tri == 0) {

    if ($rep->galeria_velkost->title == "") {
        $g_size = "";
    } elseif ($rep->galeria_velkost->title == "size20x") {
        $g_size = "size20x";
    } elseif ($rep->galeria_velkost->title == "size30x") {
        $g_size = "size30x";
    } elseif ($rep->galeria_velkost->title == "size40x") {
        $g_size = "size40x";
    } elseif ($rep->galeria_velkost->title == "size50x") {
        $g_size = "size50x";
    }  else { $g_size = "";}
}

// Produkty showcase

// nastavime ak je file v produktoch aby sa nedal podnim
$uzjevproduktoch = 0;
if ($string == "r_produkty" || $tri == 0) {
    if ($rep->product_text || count($rep->product_images)) {
    $flipswitch = "";
    if ($rep->product_flip == 1) {
        $flipswitch = "style='flex-direction:row-reverse;'";
    }

    echo "
    <div $flipswitch id='product_gallery'>
        <div class='product_text'>
            
        <div id='akcia_wrapper'>";
            foreach ($rep->repeat_akcia as $akcia) {
                $farba = $akcia->akcia_global_color;
                if ($farba) {
                    //najskor vycistit input, aby tam neboli medzeri, tabulatory, atd,
                    //$clean_input = preg_replace('/\s/u', '', $farba); 
                
                    //rozdelit hodnoty podla ciarky, jeden input za ciarkou atd.
                    $farby = explode("|", strip_tags($farba));
                
                    echo "
                    <div style='background-color:$farby[0];' class='product_akica'>
                    <span style='color:$farby[1];'>$farby[2]</span>
                    </div>";
                }
            }
    echo "
        </div>

        $rep->product_text 
        <div class='product_ceny'>
            <small>$rep->orientacna_cena</small>
            <h1>".$rep->cena."</h1>
            <div class='product_butoniky'>";
                foreach ($rep->buttoniky as $button) {
                    if ($button->product_ref && $button->product_button_text) {
                        echo "<a class='button ".$button->button_style->title."' href='".$button->product_ref->url."'>$button->product_button_text</a>";
                    }
                }
            echo "</div>
        </div>
        
        </div>
        <div class='product_wrapper'><div class='product_images' id='image_gallery'><div class='product_main'>
        ";
        $i = 1; 
        foreach($rep->product_images as $p_images) {
            $thumb = $p_images->size(0, 260);
            $large = $p_images->webpUrl;
                echo "
                    <figure itemprop='associatedMedia' itemscope='' itemtype='http://schema.org/ImageObject'>
                    <a href='$large' class='hvr-grow' itemprop='contentUrl' data-size='{$p_images->width}x{$p_images->height}'>
                    <img itemprop='thumbnail' loading='lazy' src='$large' alt='$p_images'>
                    </a>
                    </figure>";

                    

        }
        echo "</div></div>";
        if (count($rep->subory)) {
            echo "<div style='margin: 0 0 35px 0;'><h2>Na stiahnutie:</h2>";
            foreach ($rep->subory as $file) {
                $bytes = $file->filesize;
                if ($bytes >= 1073741824) {
                    $bytes = number_format($bytes / 1073741824, 2) . ' GiB';
                } elseif ($bytes >= 1048576) {
                    $bytes = number_format($bytes / 1048576, 2) . ' MiB';
                } elseif ($bytes >= 1024) {
                    $bytes = number_format($bytes / 1024, 2) . ' KiB';
                } elseif ($bytes > 1) {
                    $bytes = $bytes . ' bytes';
                } elseif ($bytes == 1) {
                    $bytes = $bytes . ' byte';
                } else {
                    $bytes = '0 bytes';
                }
                echo "<a style='color:var(--textovka);font-size:18px;' class='grid-test download' href='{$file->url}'><i class=\"fa fa-file-pdf-o\"></i><div><span>{$file->name}</span><span>({$bytes})</span></div> </a><br>";
            }
            echo "</div>";
            $uzjevproduktoch = 1;
        }
        echo "</div>
        
    </div>";
    }
}


// GALERIA
if ($string == "r_gallery" || $tri == 0) {
    if (count($rep->r_gallery)) {
        $gal5 = "";
        if ($rep->verticalgallery == 1) {
            $gal5 = "grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));";
        }
        if ($rep->galeria_klik == 1) {
            echo "<div style='$gal5' class='image_gallery gale $g_size '>";
        } else {
            echo "<div style='$gal5' id='image_gallery' class='image_gallery gale $g_size '>";
        }
        foreach ($rep->r_gallery as $image) {
            $options = array('quality' => 80, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
            #$large = $image->size(1700, 0);
            #$size40x = $image->size(762, 0);
            #$size50x = $image->size(1470, 0);
            #$thumb = $image->size(428.57, 0);
            # zpoznakovanie kvoli optimalizacii
            $thumb = $image->size(0, 260);
            $size20x = $image->webpUrl;
            $size30x = $image->webpUrl;
            $size40x = $size30x;
            $size50x = $size30x;

            $style = "";
            if ($rep->verticalgallery == 1 && $rep->galeria_velkost->title !== "1ku1format" ) {
                $style = "style='aspect-ratio:10/16;'";
                $thumb = $image->size(0, 860);
            } elseif ($rep->galeria_velkost->title == "1ku1format") {
                $style = "style='aspect-ratio:1/1;'";
            }
            
            if ($rep->galeria_klik == 1) {
            echo "
            <figure $style>
                <a href='javascript:void(0);' >
                ";
                if ($g_size == "size50x") {
                    echo "<img loading='lazy' src='$size50x' alt='$image'>";
                } elseif ($g_size == "size40x") {
                    echo "<img loading='lazy' src='$size40x' alt='$image'>";
                } elseif ($g_size == "size30x") {
                    echo "<img loading='lazy' src='$size30x' alt='$image'>";
                } elseif ($g_size == "size20x") {
                    echo "<img loading='lazy' src='$size20x' alt='$image'>";
                } else {
                    echo "<img loading='lazy' src='$thumb->url' alt='$image'>";
                }
            } else {
            echo "
            <figure $style itemprop='associatedMedia' itemscope itemtype='http://schema.org/ImageObject'>
                <a href='$image->webpUrl' class='hvr-grow' itemprop='contentUrl' data-size='{$image->width}x{$image->height}'>
                ";
                if ($g_size == "size50x") {
                    echo "<img itemprop='thumbnail' loading='lazy' src='$size50x' alt='$image'>";
                } elseif ($g_size == "size40x") {
                    echo "<img itemprop='thumbnail' loading='lazy' src='$size40x'  alt='$image'>";
                } elseif ($g_size == "size30x") {
                    echo "<img itemprop='thumbnail' loading='lazy' src='$size30x' alt='$image'>";
                } elseif ($g_size == "size20x") {
                    echo "<img itemprop='thumbnail' loading='lazy' src='$size20x' alt='$image'>";
                } else {
                    echo "<img itemprop='thumbnail' loading='lazy' src='$thumb->url' alt='$image'>";
                }
            }

            echo "
            </a>";
                if ($image->description) {
                    echo "<figcaption  itemprop='caption description'>$image->description</figcaption>";
                }
                echo "
            </figure>
            ";
            #$image->removeVariations();
        }
        echo "</div>";
    }
}

//mozaika
if ($string == "obr_mozaic" || $tri == 0) {
    if (count($rep->obr_mozaic)) {
        $bunka_size = $rep->velkost_bunky->title;
        echo "<div  class='mosaic_gallery b$bunka_size gale'>";
        foreach ($rep->obr_mozaic as $image) {
            $options = array('quality' => 80, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
            #$large = $image->size(1700, 0);
            #$size40x = $image->size(762, 0);
            #$size50x = $image->size(1470, 0);
            #$thumb = $image->size(428.57, 0);
            # zpoznakovanie kvoli optimalizacii
            #
            $thumb = $image;
            $size20x = $image->url;
            $size30x = $image->url;
            #$size30x = $image->webpUrl;webp v mozaike uz nejde, lebo musi to mat file field kvoli videa a file field nepodporuje variace obrazkov
            $size40x = $size30x;
            $size50x = $size30x;

            $bunkapos = explode(" ",$image->description);
            
            if ($rep->galeria_klik == 1) {
            echo "
            <figure>
                <a href='javascript:void(0);' >
                ";
                if ($g_size == "size50x") {
                    echo "<img loading='lazy' src='$size50x' alt='$image'>";
                } elseif ($g_size == "size40x") {
                    echo "<img loading='lazy' src='$size40x' alt='$image'>";
                } elseif ($g_size == "size30x") {
                    echo "<img loading='lazy' src='$size30x' alt='$image'>";
                } elseif ($g_size == "size20x") {
                    echo "<img loading='lazy' src='$size20x' alt='$image'>";
                } else {
                    echo "<img loading='lazy' src='$thumb->url' alt='$image'>";
                }
                echo "
                </a>
            </figure>";
            } else {
                echo "<div id='image_gallery' style='grid-row:span $bunkapos[1];grid-column:span $bunkapos[0];'>";
                $size = getimagesize($image->httpUrl);
                if (substr($image, -3) == "mp4") {
                    echo "<a class='hvr-grow' data-rel=lightcase:videjko' href='";



                    if (sizeof($bunkapos) == 3) {
                        $url = $bunkapos[2];
                        $urls = parse_url($url);
                        $typ_videa = 'ziadne';
                        if (preg_match('%youtube|youtu\.be%i', $urls['host'])) {
                            $typ_videa = 'youtube';
                        } elseif (preg_match('%vimeo%i', $urls['host'])) {
                            $typ_videa = 'vimeo';
                        } else {
                            continue;
                        }
                        // echo $typ_videa;
                        //Expect the URL to be http://youtu.be/abcd, where abcd is the video ID
                        if ($urls['host'] == 'youtu.be') :
                            $imgPath = ltrim($urls['path'], '/');
                        //Expect the URL to be http://www.youtube.com/embed/abcd
                        elseif (strpos($urls['path'], 'embed') == 1) :
                            $imgPath = end(explode('/', $urls['path']));
                        //Expect the URL to be http://www.youtube.com/shorts/abcd
                        elseif (strpos($urls['path'], 'shorts') == 1) :
                            $temper = explode('/', $urls['path']);
                            $imgPath = end($temper);
                        //Expect the URL to be abcd only
                        elseif (strpos($url, '/') === false) :
                            $imgPath = $url;
                        //Expect the URL to be http://www.youtube.com/watch?v=abcd
                        else :
                            parse_str($urls['query'], $v);
                            $imgPath = implode("",$v);
                        endif;


                        if ($urls['host'] == 'youtu.be') :
                            $url = "https://www.youtube.com/watch?v={$imgPath}";
                        endif;
                        $url = "https://www.youtube.com/embed/{$imgPath}?&autoplay=1";
                        echo $url;
                    } 
                    echo "'><video autoplay muted playsinline loop> <source  src='$image->url' type='video/mp4'></video>";
                    echo "</a></div>";
                } else {
                    echo "
                    <figure  itemprop='associatedMedia' itemscope itemtype='http://schema.org/ImageObject'>";        
                    echo "<a href='$image->url' class='hvr-grow' itemprop='contentUrl' data-size='{$size[0]}x{$size[1]}'>
                    ";
                    if ($g_size == "size50x") {
                        echo "<img itemprop='thumbnail' loading='lazy' src='$size50x' alt='$image'>";
                    } elseif ($g_size == "size40x") {
                        echo "<img itemprop='thumbnail' loading='lazy' src='$size40x'  alt='$image'>";
                    } elseif ($g_size == "size30x") {
                        echo "<img itemprop='thumbnail' loading='lazy' src='$size30x' alt='$image'>";
                    } elseif ($g_size == "size20x") {
                        echo "<img itemprop='thumbnail' loading='lazy' src='$size20x' alt='$image'>";
                    } else {
                        echo "<img itemprop='thumbnail' loading='lazy' src='$thumb->url' alt='$image'>";
                    }
                    echo "
                    </a>";
                    echo "
                    </figure></div>
                    ";
                }  


            }
            #$image->removeVariations();
        }
        echo "</div>";
    }
}

// VIDEO
if ($string == "home_page_video_link" || $tri == 0) {
    $video_pw = "$rep->home_page_video_link";
    /* bol este row class v video_grid dals om prec kvol ikompatibilite */
    $video_div = "<div class=' gallery image_gallery $g_size' id='video_grid'>";
    include('chunks/video.php');
}

//SUBORY
if ($string == "subory" || $tri == 0) {
    if (count($rep->subory) && $uzjevproduktoch == 0) {
        echo "<div style='margin: 0 0 35px 0;'><h2>Na stiahnutie:</h2>";
        foreach ($rep->subory as $file) {
            $bytes = $file->filesize;
            if ($bytes >= 1073741824) {
                $bytes = number_format($bytes / 1073741824, 2) . ' GiB';
            } elseif ($bytes >= 1048576) {
                $bytes = number_format($bytes / 1048576, 2) . ' MiB';
            } elseif ($bytes >= 1024) {
                $bytes = number_format($bytes / 1024, 2) . ' KiB';
            } elseif ($bytes > 1) {
                $bytes = $bytes . ' bytes';
            } elseif ($bytes == 1) {
                $bytes = $bytes . ' byte';
            } else {
                $bytes = '0 bytes';
            }
            echo "<a style='color:var(--textovka);font-size:18px;' class='grid-test download' href='{$file->url}'><i class=\"fa fa-file-pdf-o\"></i><div><span>{$file->name}</span><span>({$bytes})</span></div> </a><br>";
        }
        echo "</div>";
    }
}
echo "</div>";
?>