<!DOCTYPE html>
<html lang="sk">
  <head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$pages->get('/')->home_title?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/css/font-awesome.min.css" rel="stylesheet" />
    <!-- <link href="<?= $config->urls->templates; ?>assets/css/hover.css" rel="stylesheet" /> -->
    <!-- <link href="<?= $config->urls->templates; ?>assets/css/animate.min.css" rel="stylesheet" /> -->
	<link href="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/css/form.min.css" rel="stylesheet">
	<link href="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/css/main_theme.min.css" rel="stylesheet" />
	<link href="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/css/app.min.css" rel="stylesheet" />
	<!-- <link href="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/css/jj.css" rel="stylesheet" /> -->

	<?php
	#if ($user->isLoggedin()) {
	#	veci
	#} else {
	#	echo '<link href="'. $config->urls->templates . 'chunks/aluwinsro-incl/assets/css/jj-old.css" rel="stylesheet" />';
	#}
	echo '<link href="'. $config->urls->templates . 'chunks/aluwinsro-incl/assets/css/jj.css" rel="stylesheet" />';
	?>

	<?php
	// ked nejaky web potrebuje vlastne css tento kod ho sem vlozi
		if (isset($put_link)){
			echo $put_link;
		}
	?>

	<!-- swiper -->
	<!-- <link href="<?= $config->urls->templates; ?>swiper/package/css/swiper.min.css" rel="stylesheet" /> -->
	<link href="<?= $config->urls->templates; ?>chunks/swiper/swiper-bundle.min.css" rel="stylesheet" />
	

	<!-- lightcase pre video -->
	<!-- <link href="<?= $config->urls->templates; ?>lightcase/src/css/lightcase.css" rel="stylesheet" /> -->
<!-- 	<link rel="stylesheet" href="<?=$config->urls->templates;?>chunks/videa/vid.min.css">
	<link rel="stylesheet" href="<?=$config->urls->templates;?>chunks/videa/lightcase/src/css/lightcase.min.css"> -->


	<!-- Photoswipe -->
<!-- 	<link rel="stylesheet" href="<?=$config->urls->templates;?>chunks/psw/dist/photoswipe.min.css">
	<link rel="stylesheet" href="<?=$config->urls->templates;?>chunks/psw/dist/default-skin/default-skin.min.css"> -->

    
    <meta name="robots" content="index, follow" />
    <meta name="description" content="<?=$pages->get('/nastavenie/')->set_popis_webu?>" />
    <meta name="keywords" content="okná, okna, dvere, plastové okná, plastove okna, plastové dvere, plastove dvere, hliníkové okná, hlinikove okna, hliníkové dvere, hlinikove dvere, vchodové dvere, vchodove dvere, balkónové dvere, balkonove dvere, prestrešenia terás, prestresenia teras" />
    <meta name="author" content="webcraft.sk" />
    <!--  <link rel="shortcut icon" href="<?=$config->urls->templates;?>images/favicon-tarasola.png" /> -->
    <link rel="shortcut icon" href="<?=$pages->get('/nastavenie/')->set_favicon->url?>" />
    

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->



<!-- testovacie css pre uzivatelov -->
<?php
if ($user->isLoggedin()) {

} // pre prihlasenych TESTOVACIE PROSTREDIE

if ($page->template->name == 'home') {
	if (count($page->m_image_slider)) {
		echo "
		<style>
		.mobile {
			display: none;
		}
		@media screen and (max-width: 991px) {
			.mobile {
				display: block;
			}
			.pc {
				display:none;
			}
		}
		</style>
		";
	}
}

?>

<?php
	// pre google analytics kod
	if (isset($put_google)){
		echo $put_google;
	}
?>	

</head>

<body>
	
<div class="content">


<header class="container-fluid box_padding header">
<div class="row">
<!-- style="width:<?=$pages->get('/nastavenie/')->logo_sirka?>px !important;" -->
<div class="col-md-3 main_logo">
<a href="/">
<?php 
if ($pages->get('/nastavenie/')->logo) {

	$image = $pages->get('/nastavenie/')->logo;
	// $options = array('quality' => 80, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
	// $large = $image->size(0, 47, $options);
	$large = $image;
	echo '<img  src="';
	echo $large->url;
	echo '" class="img-responsive" alt="Aluwin" />';
}

?><div><?=$pages->get('/nastavenie/')->logo_text?></div></a>
</div>

<?php 
if (!isset($custom_header)):
?>

	<div class="col-md-9">
	<ul>
	<?php
	$ikonky = $pages->get('/nastavenie/')->repeat_social;
	foreach($ikonky as $media) {
		$soc = "{$media->r_soc->title}";
		$cap = ucfirst($soc);
		echo "<li><a href='$media->r_url' target='_blank' rel='noreferrer' ><i class='fa fa-$soc'></i></a></li>  ";
	}
	/* target='_blank' */
	?>
	</ul>
	</div>
	</div>
	</header><!-- /HEADER -->



	<nav class="navigation">
	<div class="navbar-header navbar-default" role="navigation">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	</button>
	</div>

	<div class="collapse navbar-collapse navbar-ex1-collapse"> 
	<ul>
	<?php
	$out = '';
	$root = $pages->get("/");
	$children = $root->children("limit=150");
	/* $children->prepend($root); */
	foreach($children as $child) {
			$out .= "<li><a href='{$child->url}'>{$child->title}</a></li>";
	}
	echo $out; 
	?>
	</ul>
	</div>
	</nav><!-- /MAIN-NAVIGATION -->

<?php 
else: 
?>
	<!-- Vsetko co je tu je len pre web kuchyne -->
	<div class="navbar-header navbar-default" role="navigation">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	</button>
	</div>


	<div id="normal-menu" class="collapse navbar-collapse navbar-ex1-collapse"> 
	<ul>
	<?php
	$out = '';
	$root = $pages->get("/");
	$children = $root->children("limit=150");
	/* $children->prepend($root); */
	foreach($children as $child) {
			$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			if (strpos($url,$child->url) !== false) {
				$active = "class='active'";
			}
			else {
				$active ="";
			}
			$out .= "<li ". $active ."><a ". $active ." href='{$child->url}'>{$child->title}</a></li>";
	}
	echo $out; 
	?>
	</ul>
	</div>


	<nav class="navigation">
	<div class="collapse navbar-collapse navbar-ex1-collapse"> 
	<ul>
	<?php
	$out = '';
	$root = $pages->get("/");
	$children = $root->children("limit=150");
	/* $children->prepend($root); */
	foreach($children as $child) {
			$out .= "<li><a href='{$child->url}'>{$child->title}</a></li>";
	}
	echo $out; 
	?>
	</ul>
	</div>
	</nav>


	</div>
	</header>

<?php
endif; 
?>
