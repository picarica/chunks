<?php 
if ($rep->include_repeat) {
    //najskor vycistit input, aby tam neboli medzeri, tabulatory, atd,
    $clean_input = preg_replace('/\s/u', '', $rep->include_repeat); 

    //rozdelit hodnoty podla ciarky, jeden input za ciarkou atd.
    $arr = explode(",", $clean_input);

    //treba a spocitat -1 lebo array zacina od 0, tak ked napisa 2 aby bol druhy repeat field
        if (is_numeric($arr[1]) && is_numeric($arr[0])) {
            $plus1 = $arr[1] - 1;
        }

    //naplni array pomocout repeat fieldu ktoreho sme vybrali
    if(isset($plus1)) {
        $rep = $pages->get($arr[0])->repeat_page[$plus1];
    }

    // zistujeme ci ma 3 inputy, lebo nemusi dat 3 stacia dve a ked ma 3 if na kazdy jeden
    if (sizeof($arr) == 3 && isset($plus1)) {
        //konvertujeme posledn input do stringu, pouziva sa aj v podmienku v repat_produkty.php
        $string = $arr[2];

        // toto pouzivame zase ako podmienku v repeat_produkty.php ze ked nejsu 3 aby sme vedeli pouzit kazdy jeden
        $tri = "1";

        // a includuje strankou pomocou vsetkych infromaciu stranku vygenerujeme
        include("./repeat_produkty.php");
    } elseif(sizeof($arr) == 2 && isset($plus1)) {
        // tu davame prazdny string, aby sme nedostali chyba ze variable je prazdny
        $string = "";

        // variabl $tri vypiname aby sme mohli vojst do kazdej podienky
        $tri = "0";

        //generujeme includovane
        include("./repeat_produkty.php");
    } else {
        // ked uzivatel zada 1 alebo 4 inputy nech vypise error
        echo "<br><h1 style='color:red;'>Include Error </h1><br><h2>Zadana hodnota nieje spravna v <span style='color:darkseagreen;'>$err</span> Repeat fielde<br>Opravte chybu a zadjte novu podla syntaxi v popisu Fieldu</h2><br><h3>Dakujem</h3><br>";
    }

}
?>