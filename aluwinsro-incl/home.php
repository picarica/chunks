<!--
	aluwin.sk by Webcraft
-->


<!-- Banner -->
<div class="rslider_wrapper swiper-container pc">
<ul class="rslides_tabs text-center swiper-wrapper ">
<?php


	foreach($page->image_slider as $image) {
		
		$options = array('quality' => 90, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
		if($image->description) {
			preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $image->description, $result);
			if (!empty($result)) {
				if (!empty($result['href'][0])){
					$link_produkt = $result['href'][0];
				} else {
					$link_produkt = "javascript:void(0)";
				}
			}
		}

		echo "<div class='swiper-slide'><a ";  
			echo "href=$link_produkt"; // <-- tu by mal byt link pre obrazok
		echo" ><img class='rslider_img' loading='lazy' src='$image->webpUrl' alt='$image'></a>"; // <--- tuje link obrazku
			if ($image->akcia_global_color_fix || $image->home_slider_text ) {
				echo "<div class='gradient-back'></div>";
			}
		if($image->description) {
			echo "
			<div class='rslider_content'>
			<div class='container text-center'>
			<div class='rslider_content_in'>$image->description</div>
			</div>
			</div>";
		}
		

			if ($image->akcia_global_color_fix || $image->home_slider_text ) {
			
				echo '
				<div class="posi">';
				if ($image->akcia_global_color_fix ) {

					$farba = $image->akcia_global_color_fix;
	
					//najskor vycistit input, aby tam neboli medzeri, tabulatory, atd,
	
					//rozdelit hodnoty podla ciarky, jeden input za ciarkou atd.
					$farby = explode("|", $farba);

				echo	'<div style="background-color:'.preg_replace('/\s/u', '', $farby[0]).';" class="home action">
						<span style="color:'.preg_replace('/\s/u', '', $farby[1]).';">'. $farby[2] .'</span>
					</div>';
				}
				if ($image->home_slider_text ) {

					echo'
					<div class="headline">
						'.$image->home_slider_text.'
					</div>
					<a class="button" href="'.$image->reference->url.'">'.$image->home_slider_button_text.'</a>';
				}
				echo '</div>
				';
			}
		echo "</div>";
	}
	?>
	</ul>
	<div class="swiper-pagination"></div>
	<!-- <div class="rslider_logo"><div>TOP<br /> DIZ<br />AJN</div></div> -->
	</div><!-- /SLIDER-->
	<div class="rslider_wrapper m-swiper-container mobile">
<ul class="rslides_tabs text-center swiper-wrapper ">

<?php
	if(count($page->m_image_slider)) {
		foreach($page->m_image_slider as $image) {
			$options = array('quality' => 90, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
			if($image->description) {
				preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $image->description, $result);
				if (!empty($result)) {
					if (!empty($result['href'][0])){
						$link_produkt = $result['href'][0];
					} else {
						$link_produkt = "javascript:void(0)";
					}
				} else {
					$link_produkt = "javascript:void(0)";
				}
			}
			if (empty($link_produkt)) {
				$link_produkt = "javascript:void(0)";
			}
		
			echo "<div class='swiper-slide mobile'><a ";  
				echo "href=$link_produkt"; // <-- tu by mal byt link pre obrazok
			echo" ><img class='rslider_img' loading='lazy' src='$image->webpUrl' alt='$image'></a>"; // <--- tuje link obrazku
				if ($image->akcia_global_color_fix || $image->home_slider_text ) {
					echo "<div class='gradient-back'></div>";
				}
			if($image->description) {
				echo "
				<div class='rslider_content'>
				<div class='container text-center'>
				<div class='rslider_content_in'>$image->description</div>
				</div>
				</div>";
			}

				if ($image->akcia_global_color_fix || $image->home_slider_text ) {
	
					$farba = $image->akcia_global_color_fix;
	
					//najskor vycistit input, aby tam neboli medzeri, tabulatory, atd,
	
					//rozdelit hodnoty podla ciarky, jeden input za ciarkou atd.
					$farby = explode("|", $farba);
				
					echo '
					<div class="posi">
						<div style="background-color:'.preg_replace('/\s/u', '', $farby[0]).';" class="home action">
							<span style="color:'.preg_replace('/\s/u', '', $farby[1]).';">'. $farby[2] .'</span>
						</div>
						<div class="headline">
							'.$image->home_slider_text.'
						</div>
						<a class="button" href="'.$image->reference->url.'">'.$image->home_slider_button_text.'</a>
					</div>
					';
				}
			echo "</div>";
		}
	}
?>
	</ul>
	<div class="swiper-pagination"></div>
	<!-- <div class="rslider_logo"><div>TOP<br /> DIZ<br />AJN</div></div> -->
	</div><!-- /SLIDER-->


<section class="container_x1">
<div class="container">
<div class="row">
<div class="col-sm-6">
<div class="title_box box_margin3"><h1><?=$page->home_sub_title?></h1></div>
<p class="box_margin"><?=$page->home_sub_text?></p>
<a href="o-nas/" class="nav_next">ZOBRAZIŤ VIAC</a>
<div class="container_main_title"><h3><?=$page->title_text?></h3></div>
</div>
<?php
	if ($page->html5_video) {
		echo '
		<div class="html5_video col-sm-6">
		<ul class="">';

		$urlvideo = $page->html5_video->url;
		$koncovka = substr($urlvideo, strrpos($urlvideo, '.' )+1)."\n";

		echo "
		<video playsinline muted autoplay loop playsinline>
		
			<source src='$urlvideo' type='video/$koncovka'>
		</video>";
	} else {
		echo '
		<div class="rslider_wrapper rslider_wrapper_sml swiper-container2">
		<ul class="rslides swiper-wrapper">';
		foreach($page->mini_slider as $image) {
			$options = array('quality' => 80, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
			$large = $image->size(1140, 760);
			echo "<div class=\"rslider_img swiper-slide\" style=\"opacity:1;background-image:url('$large->webpUrl');\">&nbsp;</div>";
		}
		echo '</ul>
		<a href="javascript:void(0)" class="text-center img-circle rslides_nav rslides1_nav prev swiper-button-prev"></a>
		<a href="javascript:void(0)" class="text-center img-circle rslides_nav rslides2_nav next swiper-button-next"></a>
		';
	}
	
	
?>
</div>
</div>
<!-- <li class="rslides_li"> -->

<!-- </li>   -->
</div><!-- /SLIDER-SML-->
</section><!-- /CONTAINER-X1-->

<section class="container container_x2">
<div class="text-center box_margin3"><h2><?=$page->showcase_title?></h2></div>
</section>
<div id="home_grid" class="text-center">
	<?php 

		
		$aa = 1;
		foreach($page->showcase_repeat as $showcase) {
			echo "<div id='item$aa' class='bi02_content'>";
			if (substr($showcase->showcase_images, -3) == "mp4") {
				echo "<a href='$showcase->showcase_link'>";
				echo "<video playsinline autoplay muted loop> <source src='{$showcase->showcase_images->url}' type='video/mp4'></video>";
			} else {
				echo "<a href='$showcase->showcase_link' style=\"background-image:url('{$showcase->showcase_images->url}')\">";
			}
			echo "<div class='bi02_title bi02_title_center'><span>$showcase->showcase_title</span></div>";
			echo "</a>";
			echo "</div>";
			$aa++;
		}
	?>
</div><!-- /CONTAINER-X2-->

<section class="container_x3">
<div class="container text-center">
<div class="row">
<div class="box_margin"><?=$page->hex_title?></div>
<p class="box_margin3"><?=$page->hex_text?></p>
<div class="box_imp01">
	
<?php 
$aa = 1; 
foreach($page->r_hex as $hexak) {
	if($aa < 6) {
	echo "<div class='box_imp01_in'><a href='javascript:void(0);'>";
	echo "<div class='hexagon_wrapper'><div class='hexagon'><span>0$aa.</span></div></div>";
	echo "<div class='box_imp01_content'><h3>$hexak->r_hex_title</h3> ";
	echo "<p>$hexak->r_hex_text</p></div>";
	echo "</a></div>";
	$aa++;
	}
}
?>

</div>
</div>
</div>
</section><!-- /CONTAINER-X3-->

<section class="container_x4">
<div class="container">
<div class="box_margin"><h2><?=$page->home_video_title?></h2></div>
<p class="box_margin"><?=$page->home_video_text?></p>
<?php
	//if ($page->home_page_video_link) {
		echo '<a style="margin-bottom: 35px;" href="'.$pages->get('/nastavenie/')->set_link_referencie .'" class="nav_next">ZOBRAZIŤ VIAC</a>';
		$video_pw = "$page->home_page_video_link";
		$video_div = "<div class='row gallery video_grid gale' id='video_grid'>";
		include('./chunks/video.php');
	//}
?>
</div>
</section><!-- /CONTAINER-X4-->

<!-- <?php
if ($page->color_field1 && $page->color_field2) {

	echo "
	<section class='container_x5'>
	<div class='container text-center'>
	<div class='row'>
	<div class='col-xs-6 c_x5 c_x5_left'>
	<?=$page->color_field1?>
	<a href='<?=$page->color_field_link1?>' class='button button01'><span>zobraziť viac</span></a>
	</div>
	<div class='col-xs-6 c_x5 c_x5_right'>
	<?=$page->color_field2?>
	<a href='<?=$page->color_field_link1?>' class='button button02'><span>zobraziť viac</span></a>
	</div>
	</div>
	</div>
	</section>";
}
?> --><!-- /CONTAINER-X5-->

<footer class="footer">
<div class="container">
<div class="row vnutro">
<div class="col-md-5 footer_in">
<?=$page->home_kontakt_title_small?>
<div class="title_box box_margin3"><?=$page->home_kontakt_title?></div>
<div class="row">


<?=$page->home_kontakt?>

</div>
</div>
</div>
</div>


<?php 
if ($pages->get('/nastavenie/')->set_mapa) {


echo "<div id=\"canvas1\"><div class=\"map_canvas1\" style='height:100%;width:100%;'><iframe src=\"";
echo $pages->get('/nastavenie/')->set_mapa;
echo " \"style='border:0; width:100%; height:100%;min-height:512px;' allowfullscreen=''></iframe>";

 if ($pages->get('/nastavenie/')->formular_off == 0) {
	 include('chunks/aluwinsro-incl/formular.php');
 }
 echo "</div>";
}
 ?>
</footer><!-- /FOOTER -->