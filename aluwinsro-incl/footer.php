<?php
if($page->id == 1) {
	if ($pages->get('/nastavenie/')->formular_off == 1) {
		echo "<footer style='margin-top:auto !important ;' class='footer_bottom'>";
	} else {
		echo "<footer class='footer_bottom'>";
	}
} else {
	echo "<footer class='footer_bottom_alt'>";
}
?>


<!-- <div class="container"> -->
<div class="row">
<div class=" footer_bottom01">
	<div class="row">

		<div class="col-sm-6">
		<?php
		$out = '';
		$root = $pages->get("/");
		$children = $root->children("limit=150");
		$aa = 1;
		/* $children->prepend($root); */
		foreach($children as $child) {
				$out .= "<li><a href='{$child->url}'>{$child->title}</a></li>";
			if($aa % 3 == 0) {
				$out .= "</div>";
				$out .= "<div class=\"col-sm-6\">";
			}
			$aa++;
		}
		echo $out; 

		?>
		</div>

	</div>
</div>
<div class=" text-center footer_bottom02">
	<h3></h3>
		<ul>
		<?php
		$ikonky = $pages->get('/nastavenie/')->repeat_social;
		foreach($ikonky as $media) {	
			$soc = "{$media->r_soc->title}";
		$cap = ucfirst($soc);
		echo "<li><a href='$media->r_url' target='_blank' rel='noreferrer'><i class='fa fa-$soc'></i></a></li>";
		}
		/* target='_blank' */
		?>
	</ul>
</div>
<div class=" text-right footer_bottom03">
	<ul>
 <li>
 <?php
	// ked nejaky web potrebuje vlastne css tento kod ho sem vlozi
		if (isset($put_copy)){
			echo $put_copy;
		} else {
			echo '<a href="https://www.aluwin.sk" rel="noreferrer" target="_blank">&copy; ';
			echo date("Y");
			echo ' by ALUWIN, spol. s r.o.</a>';
			
		}
?>
</li>

	</ul>
</div>

</div>
<!-- </div> -->
</footer><!-- /FOOTER-BOTTOM -->

</div><!-- /CONTENT-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/js/bootstrap.min.js"></script>

	<!-- swiper -->
	<!-- <script src="<?= $config->urls->templates; ?>swiper/package/js/swiper.min.js" ></script> -->
	<script src="<?= $config->urls->templates; ?>chunks/swiper/swiper-bundle.min.js" ></script>

	<!-- lightclasee -->
	<script type="text/javascript" src="<?=$config->urls->templates;?>chunks/videa/lightcase/src/js/lightcase.min.js"></script>
	<script type="text/javascript" src="<?=$config->urls->templates;?>chunks/videa/lightcase/vendor/jQuery/jquery.events.touch.min.js"></script>

	<!-- Map -->


	<!-- Photoswipe -->
	<script src="<?=$config->urls->templates;?>chunks/psw/dist/photoswipe.min.js"></script>
	<script src="<?=$config->urls->templates;?>chunks/psw/dist/photoswipe-ui-default.min.js"></script>
	<!-- selector pre photoswipe je id="image_gallery" -->
	<script src="<?=$config->urls->templates;?>chunks/psw/photoswipe-init-fade-open.min.js"></script>
	

	

    <!-- Wow -->
    <script src="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/js/wow.min.js"></script>
    <script>
       new WOW().init();
    </script>
				
			


<script type="text/javascript">
function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

let isMobile = window.matchMedia("only screen and (max-width: 1000px)").matches;


ready(function() {
var phplink = "<?= $config->urls->templates; ?>" ;
var links = [ 
phplink + "chunks/psw/dist/photoswipe.min.css",  
phplink + "chunks/psw/dist/default-skin/default-skin.min.css", 
phplink + "chunks/videa/vid.min.css", 
phplink + "chunks/videa/lightcase/src/css/lightcase.min.css", 
phplink + "chunks/aluwinsro-incl/assets/css/font-awesome.min.css", 

];
var types = [
"css", 
"css", 
"css", 
"css", 
"css", 
 ];
for (var i = 0; i < links.length; i++) {
	if (types[i] == "css") {
		var tag = document.createElement("link");
		var attr = document.createAttribute("rel");
		attr.value = "stylesheet";
		tag.setAttributeNode(attr);
		tag.href = links[i];
		document.getElementsByTagName("head")[0].appendChild(tag);
	} else {
		var tag = document.createElement("script");
		var container = document.getElementsByClassName("container_x1")[0];
		tag.src = links[i];
		container.appendChild(tag);
	}
}




	$('a[data-rel^=lightcase]').lightcase({
		swipe: true,
		controls : true,
		autobuffer : true,
		speedIn:0,
		speedOut:0,
		maxWidth:3840,
		maxheight:2160,
		onStart : {
			function() {
				var i, iframe;
				iframe = document.querySelector(".lightcase-contentInner > iframe");
				iframe.setAttribute('allowFullScreen', '')
				iframe.setAttribute('webkitallowfullscreen', '')
				iframe.setAttribute('mozallowfullscreen', '')
				var zdroj = iframe.src ;
				iframe.removeAttribute("src") ;
				iframe.removeAttribute("style") ;
				iframe.src = zdroj ;
			}
		},
		onFinish: {
			function() {
				var i, iframe;
				iframe = document.querySelector(".lightcase-contentInner > iframe");
				iframe.removeAttribute("style") ;
				
			}
		},
	});
	var body;
	body = document.querySelector("body");
	body.onresize = function(){
		var i, iframe;
		iframe = document.querySelector(".lightcase-contentInner > iframe");
		iframe.removeAttribute("style") ;
	}; 

	var mySwiper = new Swiper('.swiper-container', {
	  // Optional parameters
	  direction: 'horizontal',
	  loop: true,
	  effect: 'slide',
	  spaceBetween: 20,

	  autoplay: {
	      delay: <?=$pages->get('/nastavenie/')->set_slider_speed;?>,
	  },
	  speed: 300,
	  simulateTouch: true,
	  // If we need pagination
	  pagination: {
	    el: '.swiper-pagination',
		clickable: true,
	  },
	})

	
	if (isMobile){
		var mySwiper = new Swiper('.m-swiper-container', {
		// Optional parameters
		direction: 'horizontal',
		loop: true,
		effect: 'slide',
		spaceBetween: 20,
	
		autoplay: {
			delay: <?=$pages->get('/nastavenie/')->set_slider_speed;?>,
		},
		speed: 300,
		simulateTouch: true,
		// If we need pagination
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		})

		var mySwiper = new Swiper('.swiper-container2', {
		  // Optional parameters
		  direction: 'horizontal',
		  loop: true,
		  effect: 'slide',
		  
		  autoplay: {
		      delay: <?=$pages->get('/nastavenie/')->set_slider_speed;?>,
		  },
		  speed: 500,
		  simulateTouch: true,
		})
		
	} else {
		var mySwiper = new Swiper('.swiper-container2', {
		  // Optional parameters
		  direction: 'horizontal',
		  loop: true,
		  effect: 'slide',
		  
		  autoplay: {
		      delay: <?=$pages->get('/nastavenie/')->set_slider_speed;?>,
		  },
		  speed: 500,
		  simulateTouch: false,
		  // Navigation arrows
		  navigation: {
		    nextEl: '.swiper-button-next',
		    prevEl: '.swiper-button-prev',
		  },
		})
	}

	
	var mySwiper = new Swiper('.pod_swiper', {
	  // Optional parameters
	  direction: 'horizontal',
	  loop: true,
	  spaceBetween: 10,
	  effect: 'slide',
	  autoplay: {
	      delay: 3500,
	  },
	  speed: 300,
	  simulateTouch: true,
	  // If we need pagination
	  pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
	  navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	  },
	  loopPreventsSliding: false,
	  loopPreventsSlide: false
	})

}) //ukoncenie document ready funkcie

</script>
				
				<!-- Formular -->
				<script src="<?= $config->urls->templates; ?>chunks/aluwinsro-incl/assets/js/classie.js"></script>
				<script>
			(function() {
				if (!String.prototype.trim) {
					(function() {
						// Make sure we trim BOM and NBSP
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}
           
				[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );


				[].slice.call( document.querySelectorAll( 'textarea.input__field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );

				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}

				function onInputBlur( ev ) {
					if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}        
			})();

</script>
</body></html>