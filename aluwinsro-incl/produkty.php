<!--
	aluwin.sk by Webcraft
-->

<!-- Main -->

<div class="content">


<div class="rslider_wrapper rslider_wrapper_in">

  <ul class="rslides callbacks callbacks1" id="slider1">
  <div class="rslider_img" style="background-image:url('<?php 

  //toto som opravil hadzalo to image to banneru
 if ($page->image_banner) {
	$large = $page->image_banner->size(1920, 0);
	echo $large->webpUrl;
 } elseif($page->thumb_image) {
	$large = $page->thumb_image->size(1920, 0);
	echo $large->webpUrl;
	// renson
} else {
	echo "chyba";
}

  ?>');">&nbsp;</div>
  </ul>
   
</div><!-- /SLIDER-->

<section class="container_x1">
<div class="container vnutro">

   <div class='breadcrumbs' role='navigation' aria-label='You are here:'><?php                                                               
                                                                                                                                                  
                // breadcrumbs are the current page's parents
				echo "<i class='fa fa-home'>&nbsp;</i>";
                foreach($page->parents() as $item) {                                                                                              
                        echo "<span><a href='$item->url'>$item->title</a> | </span> ";                                                               
                }                                                                                                                                 
                // optionally output the current page as the last item                                                                            
                echo "<span class='current_page'>$page->title</span> ";                                                                                                
                                                                                                                                                  
        ?></div> 

<?php 

if($page->url == '/kontakt/') {
	if ($pages->get('/nastavenie/')->formular_off == 0) {
		echo '<div class="tester col-md-12 col-lg-6">';
	}
	$err = 1;
	foreach($page->repeat_page as $rep) {
		// davamy prazdny string aby sme nemali chybu ze variable nieje definovany v repeat_produkty.php
		$string = "";

		//variable tri davame na 0, lebo kym este nechekujeme kym chcem daco includovat, nech to vygeneruje vsetko
		$tri = 0;
		include('./repeat_produkty.php');
		include('./include-gal.php');
		$err++;
	}
	if ($pages->get('/nastavenie/')->formular_off == 0) {
		echo "</div>
		<div class=\"row\" style='display:unset;'>";
		echo "<div class=\"col-md-12 col-lg-6\">";
	 	include('./formular.php');
	} else {
		echo "<div style='padding:0;' class=\"col-md-12 col-lg-12\">";
	}

	 if($pages->get('/nastavenie/')->set_mapa) {
		echo"<div class=\"e2_htmlcontent\"><iframe src=\"";
		echo $pages->get('/nastavenie/')->set_mapa;
		echo "\" style='border:0; width:100%; height:360px;' allowfullscreen=\"\"></iframe></div>";
	}
	echo "</div> ";
} elseif($page->hasChildren()) {

	echo "<h1>$page->title</h1>";

	// tato podmienka len zmeni poradie vjakom sa generuja stranka kde su aj podstranky
	if($page->switch == 1) {
	
		// tu je generacia ak stranka ma deti
	include('./child-incl.php');
	
		echo "
		<div class='vnutro'>	
			<div class='e2_htmlcontent'>";
	
			$err = 1;
			foreach($page->repeat_page as $rep) {
				// davamy prazdny string aby sme nemali chybu ze variable nieje definovany v repeat_produkty.php
				$string = "";
	
				//variable tri davame na 0, lebo kym este nechekujeme kym chcem daco includovat, nech to vygeneruje vsetko
				$tri = 0;
				include('./repeat_produkty.php');
				include('./include-gal.php');
				$err++;
			}
		echo "</div></div>";
	
	
	
	} else {
		echo "
		<div class='vnutro'>	
			<div class='e2_htmlcontent'>";
	
			$err = 1;
			foreach($page->repeat_page as $rep) {
				// davamy prazdny string aby sme nemali chybu ze variable nieje definovany v repeat_produkty.php
				$string = "";
	
				//variable tri davame na 0, lebo kym este nechekujeme kym chcem daco includovat, nech to vygeneruje vsetko
				$tri = 0;
				include('./repeat_produkty.php');
				include('./include-gal.php');
				$err++;
			}
		echo "</div></div>";
	
	// tu je generacia ak stranka ma deti
	include('./child-incl.php');
	}

} else {
echo "
<h1>$page->title</h1>
<div class='vnutro'>	
	<div class='e2_htmlcontent'>";

	$err = 1;
	foreach($page->repeat_page as $rep) {
		// davamy prazdny string aby sme nemali chybu ze variable nieje definovany v repeat_produkty.php
		$string = "";

		//variable tri davame na 0, lebo kym este nechekujeme kym chcem daco includovat, nech to vygeneruje vsetko
		$tri = 0;
		include('./repeat_produkty.php');
		include('./include-gal.php');
		$err++;
	}

			echo "
	</div>	
</div>";
}



?>



<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. 
            PhotoSwipe keeps only 3 of them in the DOM to save memory.
            Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>
			
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

