<?php
echo "<div class='image_gallery gal_pro'>";
		foreach ($page->children() as $child) {
			if ($child->tag) {
				echo "</div>";
				echo "<div class='vnutro'>	
				<div class='e2_htmlcontent'>";
				echo "<h1>" . $child->tag . "</h1>";
				echo "</div></div>";
				echo "<div class='image_gallery gal_pro'>";
			}

			$options = array('quality' => 80, 'upscaling' => true, 'cropping' => 'north', 'sharpening'=>'medium');
			#$thumb = $child->thumb_image->size(400, 350);
			$thumb = $child->thumb_image->size(600, 350);
			echo "
			<a href='$child->url' class='hvr-grow'>
			<div class='novinky_obrazok hvr-grow'>";
				$farba = $child->akcia_global_color;
				if ($farba) {
					#zmazene <p></p> tagy co tam processwire dava
					$noemptylines = preg_replace('/<p[^>]*>\s*|\s*<\/p>/', '', $farba);
					# rozdeline podla novej liny akcie
					$lines = explode("\n", $noemptylines);
					
					#nastavime is offset z hora
					$topPosition = 15;

					foreach ($lines as $line) {
						if (isset($line[2])) {

							// Extract content inside <p> tags
							$content = trim(strip_tags($line, '<p>'));
							// Split the content into an array using '|'
							$farby = explode("|", $content);
							
							// Output the information with dynamic top position
							echo "
							<div style='top:${topPosition}px;background-color:$farby[0];' class='action'>
								<span style='color:$farby[1];'>$farby[2]</span>
							</div>";

							// Increment the top position for the next iteration
							$topPosition += 35; 
						}
					}
				}
			echo "
				<img src='$thumb->webpUrl' alt='$child->title'>
			</div>
			<div class='novinky_popis matchHeight' style='height: 34px;'><b>$child->title</b><br></div></a>
			</a>
			";
		}
	echo "</div>";
    ?>